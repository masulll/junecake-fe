/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  compiler: {
    // ssr and displayName are configured by default
    styledComponents: true,
  },
  images: {
    domains: ["igrskdzqqnxqrnrgzktm.supabase.co", "bit.ly"],
  },
};

module.exports = nextConfig;
